<?php

use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\StatusController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// status controller that gives us basic info about the app.
Route::get('/status', [StatusController::class, 'index']);

Route::post('/auth', [AuthController::class, 'index']);

# since we're doing machine to machine token validation, we use just client middleware.
Route::middleware(['client'])->group(function() {
    Route::get('/validate-token', [AuthController::class, 'index']);
});
