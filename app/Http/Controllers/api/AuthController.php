<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    /**
     * This method only ever runs if we make it past the bearer token auth.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $bearerToken = $request->bearerToken();

        if ($bearerToken) {
            // split the bearer token received into header, payload and sig
            list($header, $payload, $signature) = explode('.', $bearerToken);

            // decode
            $decodedPayload = json_decode(base64_decode($payload), true);

            // return decoded payload
            return response()->json($decodedPayload);
        } else {
            // if no bearer token then just return unauth'd
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
}
