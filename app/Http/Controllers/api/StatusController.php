<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * returns a status page and the name of what this service does
     *
     */
    public function index()
    {
        // build our dto
        $status = [
            'status' => 200,
            'app_name' => env('APP_NAME'),
            'description' => 'This application is for allowing our other microservices to authenticate.'
        ];

        return response($status);
    }
}
