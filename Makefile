all:
	docker build -t auth-service:latest -f docker/Dockerfile .

configure:
	php artisan passport:install --force && php artisan passport:client

clear:
	php artisan route:clear && php artisan config:clear && php artisan cache:clear && php artisan optimize
