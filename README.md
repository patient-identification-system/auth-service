
# Authentication Service

The authentication service for the MedVerify application stack, uses Laravel Passport to generate bearer tokens through client ids and secrets.

Dependencies:
- PHP 8.1
- Laravel 10
- PHP Extensions